<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xmcda="http://www.decision-deck.org/2019/XMCDA-3.1.1"
                xmlns:xmcdav4="http://www.decision-deck.org/2021/XMCDA-4.0.0"
                exclude-result-prefixes="xmcdav4">
<!-- exclude-result-prefixes avoids the namespace to be copied into the result element when matching xmcdav4:... -->
    <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <!-- root element -->
    <xsl:template match="/*">
        <xsl:element name="xmcda:XMCDA" namespace="http://www.decision-deck.org/2019/XMCDA-3.1.1" >
            <!-- copy attributes and overwrite the schema location -->
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="xsi:schemaLocation">http://www.decision-deck.org/2019/XMCDA-3.1.1 http://www.decision-deck.org/xmcda/_downloads/XMCDA-3.1.1.xsd</xsl:attribute>
            <xsl:apply-templates select="*[name()='alternatives']"/>
            <xsl:apply-templates select="*[name()='alternativesSets']"/>
            <xsl:apply-templates select="*[name()='criteria']"/>
            <xsl:apply-templates select="*[name()='criteriaSets']"/>
            <xsl:apply-templates select="*[name()='categories']"/>
            <xsl:apply-templates select="*[name()='categoriesSets']"/>
            <xsl:apply-templates select="*[name()='performanceTable']"/>
            <xsl:apply-templates select="*[name()='alternativesValues']"/>
            <xsl:apply-templates select="*[name()='alternativesSetsValues']"/>
            <xsl:apply-templates select="*[name()='alternativesLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='alternativesSetsLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='alternativesMatrix']"/>
            <xsl:apply-templates select="*[name()='alternativesSetsMatrix']"/>
            <xsl:apply-templates select="*[name()='criteriaFunctions']"/>
            <xsl:apply-templates select="*[name()='criteriaScales']"/>
            <xsl:apply-templates select="*[name()='criteriaThresholds']"/>
            <xsl:apply-templates select="*[name()='criteriaValues']"/>
            <xsl:apply-templates select="*[name()='criteriaSetsValues']"/>
            <xsl:apply-templates select="*[name()='criteriaLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='criteriaSetsLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='criteriaMatrix']"/>
            <xsl:apply-templates select="*[name()='criteriaSetsMatrix']"/>
            <xsl:apply-templates select="*[name()='criteriaHierarchy']"/>
            <xsl:apply-templates select="*[name()='criteriaSetsHierarchy']"/>
            <xsl:apply-templates select="*[name()='alternativesCriteriaValues']"/>
            <xsl:apply-templates select="*[name()='categoriesProfiles']"/>
            <xsl:apply-templates select="*[name()='alternativesAssignments']"/>
            <xsl:apply-templates select="*[name()='categoriesValues']"/>
            <xsl:apply-templates select="*[name()='categoriesSetsValues']"/>
            <xsl:apply-templates select="*[name()='categoriesLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='categoriesSetsLinearConstraints']"/>
            <xsl:apply-templates select="*[name()='categoriesMatrix']"/>
            <xsl:apply-templates select="*[name()='categoriesSetsMatrix']"/>
            <xsl:apply-templates select="*[name()='programParameters']"/>
            <xsl:apply-templates select="*[name()='programExecutionResult']"/>
        </xsl:element>
    </xsl:template>

    <!-- "Identity" templates -->
    <!-- Move all elements into the 4.0.0 namespace: in 3.x only the root
         element is in the XMCDA-3.x namespace -->
    <xsl:template match="xmcdav4:*">
        <!-- Using match="node()", above, would select text nodes, 
             which have no local-name -->
        <xsl:element name="{local-name()}">
            <xsl:apply-templates select="@* | node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="@*">
        <xsl:copy>
            <xsl:apply-templates />
        </xsl:copy>
    </xsl:template>

    <!-- transform v4 to v3 -->
    <!-- xxxValues → xxxValue -->
    <xsl:template match="xmcdav4:alternativeValues">
        <alternativeValue>
            <xsl:copy-of select="@*"/><xsl:apply-templates select="node()" />
        </alternativeValue>
    </xsl:template>
    <xsl:template match="xmcdav4:criterionValues">
        <criterionValue>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criterionValue>
    </xsl:template>
    <xsl:template match="xmcdav4:categoryValues">
        <categoryValue>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </categoryValue>
    </xsl:template>
    <!-- xxxSetValues → xxxSetValue -->
    <xsl:template match="xmcdav4:alternativesSetValues">
        <alternativesSetValue>
            <xsl:copy-of select="@*"/><xsl:apply-templates select="node()" />
        </alternativesSetValue>
    </xsl:template>
    <xsl:template match="xmcdav4:criteriaSetValues">
        <criteriaSetValue>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criteriaSetValue>
    </xsl:template>
    <xsl:template match="xmcdav4:categoriesSetValues">
        <categoriesSetValue>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </categoriesSetValue>
    </xsl:template>
    <!-- criterionXxx -->
    <xsl:template match="xmcdav4:criterionFunctions">
        <criterionFunction>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criterionFunction>
    </xsl:template>
    <xsl:template match="xmcdav4:criterionThresholds">
        <criterionThreshold>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criterionThreshold>
    </xsl:template>
    <xsl:template match="xmcdav4:criterionScales">
        <criterionScale>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </criterionScale>
    </xsl:template>

    <xsl:template match="xmcdav4:programParameter">
        <parameter>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" />
        </parameter>
    </xsl:template>

    <!-- keep comments -->
    <xsl:template match="@* | node()" priority="-1">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
